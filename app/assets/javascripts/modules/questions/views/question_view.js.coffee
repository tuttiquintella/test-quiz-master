App.module "Questions", (Questions) ->

  class Questions.QuestionView extends Marionette.ItemView
    template: 'questions/templates/question'

    ui:
      alert: '.alert'
      entered_answer: '#answer'

    events:
      'submit form': 'submit'

    modelEvents:
      error: 'answerError'
      sync: 'modelSync'

    submit: (e) ->
      e.preventDefault()
      @model.save
        entered_answer: @ui.entered_answer.val()

    answerError: ->
      @ui.alert.text("Your answer is wrong, try again...")
      @ui.alert.removeClass('alert-success')
      @ui.alert.addClass('alert-danger')
      @ui.alert.removeClass('hide')

    modelSync: ->
      @ui.alert.text("Congratulations! Your answer is right!")
      @ui.alert.removeClass('alert-danger')
      @ui.alert.addClass('alert-success')
      @ui.alert.removeClass('hide')
