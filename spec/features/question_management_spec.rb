feature "Question management", :js => true, :driver => :poltergeist do
  scenario "User creates a valid new question" do
    visit "/questions/new"

    fill_in "question", :with => "What's the meaning of life?"
    fill_in "answer", :with => "42"
    click_button "Save" 

    expect(page).to have_selector('li', text: "What's the meaning of life?")
  end

  scenario "User tries to create a question without an answer" do
    visit "/questions/new"

    fill_in "question", :with => "What's the meaning of life?"
    click_button "Save"

    expect(page).to have_text("There was an error saving your question.")
  end

  scenario "User tries to create an empty question" do
    visit "/questions/new"

    fill_in "answer", :with => "42"
    click_button "Save"

    expect(page).to have_text("There was an error saving your question.")
  end
end